import java.util.Random;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.opengl.GL11.*;

public class DisplayWindow{

	public static void main(String[] args) {
		initDisplay();
		gameLoop();
		cleanUp();
	}

	public static void gameLoop(){

		// create a new camera
		Camera cam = new Camera(70,(float)Display.getWidth()/(float)Display.getHeight(),0.3f,1000);


		while(!Display.isCloseRequested())
		{
			// booleans for movement
			boolean forward = Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_UP);
			boolean backward = Keyboard.isKeyDown(Keyboard.KEY_S) || Keyboard.isKeyDown(Keyboard.KEY_DOWN);
			boolean left = Keyboard.isKeyDown(Keyboard.KEY_A);
			boolean right = Keyboard.isKeyDown(Keyboard.KEY_D);

			// moves forward
			if(forward){
				cam.moveZ(0.005f);
			}
			// moves back
			if(backward){
				cam.moveZ(-0.005f);
			}
			// moves left
			if(left){
				cam.moveX(0.005f);
			}
			// moves right
			if(right){
				cam.moveX(-0.005f);
			}

			// used to rotate left
			if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
				cam.rotateY(-0.05f);
			}
			// used to rotate right
			if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
				cam.rotateY(0.05f);
			}

			glClear(GL_COLOR_BUFFER_BIT); //clear colour buffer
			glClear(GL_DEPTH_BUFFER_BIT); // clear the depth buffer
			glLoadIdentity(); // reset model view matrix
			cam.useView(); // method in camera for rotating
			drawCube(5,0,0);
			drawCube(-5,0,0);
			drawBackground(); // calls drawBackground method
			Display.update(); // update the view
		}
	}

	public static void drawCube(int x,int y, int z){

		// creating the cube
		glPushMatrix();{
			glColor3f(0.6f,0.6f,0.6f);
			glTranslatef(x,y,z);
			glBegin(GL_QUADS);{
				//FrontFace
				glVertex3f(-1,-1,1);
				glVertex3f(1,-1,1);
				glVertex3f(1,1,1);
				glVertex3f(-1,1,1);

				//BackFace
				glVertex3f(-1,-1,-1);
				glVertex3f(-1,1,-1);
				glVertex3f(1,1,-1);
				glVertex3f(1,-1,-1);

				//BottomFace
				glVertex3f(-1,-1,-1);
				glVertex3f(-1,-1,1);
				glVertex3f(-1,1,1);
				glVertex3f(-1,1,-1);

				//TopFace
				glVertex3f(1,-1,-1);
				glVertex3f(1,-1,1);
				glVertex3f(1,1,1);
				glVertex3f(1,1,-1);

				//LeftFace
				glVertex3f(-1,-1,-1);
				glVertex3f(1,-1,-1);
				glVertex3f(1,-1,1);
				glVertex3f(-1,-1,1);

				//Right Face
				glVertex3f(-1,1,-1);
				glVertex3f(1,1,-1);
				glVertex3f(1,1,1);
				glVertex3f(-1,1,1);
			}
			glEnd();
		}
		glPopMatrix();
	}

	//	public static void drawRandom(){
	//		Random r = new Random();
	//		int x = 0;
	////		int randomNumber = r.nextInt(1); // we will have 100 objects 
	//		for(int i = 1; i <=1; i++){
	//		x = r.nextInt(5);
	//		System.out.println(x);
	//		}
	//		drawCube();
	//	}

	public static void drawBackground(){
	
		// new quad for the sky
		glBegin(GL_QUADS);
		glColor3f(0f, 0f, 1.0f);
		glVertex3i(500,1,500);
		glVertex3i(-500,1,500);
		glVertex3i(-500,1,-500);
		glVertex3i(500,1,-500);
		glEnd();

		//new quad for straight road
		glBegin(GL_QUADS);
		glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-1,-0.75f,-50);
		glVertex3f(1,-0.75f,-50);
		glVertex3f(1,-0.75f,50);
		glVertex3f(-1,-0.75f,50);
		glEnd();

		//new quad for the road turning left
		glTranslatef(0,0,-50);
		glTranslatef(-49,0,0);
		glBegin(GL_QUADS);
		glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-50,-0.75f,-1);
		glVertex3f(50,-0.75f,-1);
		glVertex3f(50,-0.75f,1);
		glVertex3f(-50,-0.75f,1);
		glEnd();

		//new quad for the 2nd straight road
		glTranslatef(0,0,50);
		glTranslatef(-49,0,0);
		glBegin(GL_QUADS);
		glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-1,-0.75f,-50);
		glVertex3f(1,-0.75f,-50);
		glVertex3f(1,-0.75f,50);
		glVertex3f(-1,-0.75f,50);
		glEnd();

		//new quad for final road
		glTranslatef(0,0,50);
		glTranslatef(49,0,0);
		glBegin(GL_QUADS);
		glColor3f(0.6f, 0.6f, 0.6f);
		glVertex3f(-50,-0.75f,-1);
		glVertex3f(50,-0.75f,-1);
		glVertex3f(50,-0.75f,1);
		glVertex3f(-50,-0.75f,1);
		glEnd();
	}


	public static void cleanUp(){
		Display.destroy();
	}

	public static void initDisplay(){
		try{
			// sets up the display
			Display.setDisplayMode(new DisplayMode(800,600));
			// makes background green for the grass
			Display.setInitialBackground(0f,0.6f,0.25f);
			Display.setTitle("Car simulator");
			Display.create();
		}
		catch (LWJGLException ex){	
		}
	}
}
