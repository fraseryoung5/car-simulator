import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;

public class Camera 
{
	// values for x,y and z
	private float x;
	private float y;
	private float z;
	// values for rotating x,y and z
	private float rx;
	private float ry;
	private float rz;

	// floats for current view, aspect ratio, and near + far values
	private float view;
	private float aspect;
	private float near;
	private float far;

	public Camera(float view, float aspect, float near, float far){
		x = 0;
		y = 0;
		z = 0;
		rx = 0;
		ry = 0;
		rz = 0;

		this.view = view;
		this.aspect = aspect;
		this.near = near;
		this.far = far;
		initProjection();
	}

	private void initProjection(){
		// setups for the camera
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(view,aspect,near,far);
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
	}
	

	public void useView(){
		glRotatef(rx,1,0,0); //rotates x axis
		glRotatef(ry,0,1,0); // rotates y axis
		glRotatef(rz,0,0,1); // rotates z axis
		glTranslatef(x,y,z); // moves camera to correct position
	}

	// method for moving along z-axis
	public void moveZ(float amt){
	z += amt * Math.sin(Math.toRadians(ry + 90));
	x += amt * Math.cos(Math.toRadians(ry + 90));
	}

	// method for moving along x-axis
	public void moveX(float amt){
	z += amt * Math.sin(Math.toRadians(ry));
	x += amt * Math.cos(Math.toRadians(ry));
	}

	// method for rotating y
	public void rotateY(float amt){
	ry += amt;
	}

	// method for rotating x
	public void rotateX(float amt){
	rx += amt;
	}

	// method for rotating z
	public void rotateZ(float amt){
	rz += amt;
	}
}